﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using UnityEngine;

public interface IDash
{
    void DashTo(Vector3 position, Action dashStart = null, Action dashComplete = null);
}


public class PlayerMovementController : MonoBehaviour, IDash
{

    [Header("Setup")]
    public bool canMove = true;

    [Header("Settings")]
    public float movementSpeed;

    [Header("Dash")]
    [SerializeField]
    private float dashDistance = 10f;
    public float dashSpeed;
    public AnimationCurve dashEaseType;

    private Tween dashTween;

    //Privates
    private bool isMoving;
    private float oldAngle, currentAngle;


    public void Move(Vector3 movePos)
    {
        if (canMove)
        {

            isMoving = movePos.sqrMagnitude > 0;

            //Adicionar ease depois
            //t += Time.deltaTime;
            //float s = t / duration;
            //transform.position = Vector3.Lerp(start.position, end.position, curve.Evaluate(s));


            var playerPosition = gameObject.transform.position;
            gameObject.transform.position = Vector3.Lerp(playerPosition, playerPosition + movePos, Time.deltaTime * movementSpeed);
        }
    }


    public void Turning(float h, float v)
    {

        if (currentAngle != 0 && currentAngle != -180)
        {
            oldAngle = currentAngle;
        }

        currentAngle = Angle(new Vector2(h, v));

        if (h != 0 || v != 0)
        {

            gameObject.transform.eulerAngles = new Vector3(0, currentAngle, 0);

        }
        else if (h == 0 && v == 0)
        {

            gameObject.transform.eulerAngles = new Vector3(0, oldAngle, 0);
        }

    }

    private float Angle(Vector2 p_vector2)
    {

        if (p_vector2.x < 0)
        {
            return 360 - (Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg * -1);
        }
        else
        {
            return Mathf.Atan2(p_vector2.x, p_vector2.y) * Mathf.Rad2Deg;
        }
    }

    public void Dash(float? dashStrength = null, Action dashStart = null, Action dashComplete = null)
    {
        var dashValue = dashDistance;

        if (dashStrength != null)
            dashValue = (float)dashStrength;

        var dashPos = gameObject.transform.position + gameObject.transform.forward * dashValue;

        DashTo(dashPos, dashStart, dashComplete);
    }


    public void DashTo(Vector3 position, Action dashStart = null, Action dashComplete = null)
    {
        //if (shakeScreen)
        //{
        //    var direction = (position - gameObject.transform.position).normalized;
        //    impulseSource.GenerateImpulse(direction * -1);
        //}


        dashTween.Kill(true);
        dashTween = transform.DOMove(position, dashSpeed).SetSpeedBased(true).SetEase(dashEaseType).OnStart(() =>
        {
            dashStart?.Invoke();

        }).OnComplete(() =>
        {

            StartCoroutine(DelayCouroutine());
            IEnumerator DelayCouroutine()
            {
                yield return new WaitForSeconds(0.05f);
                dashComplete?.Invoke();
            }

        });
    }


    public void StopDash()
    {
        dashTween.Kill();
    }

}
