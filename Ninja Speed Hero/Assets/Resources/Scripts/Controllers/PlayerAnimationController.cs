﻿using System;
using Sirenix.OdinInspector;
using UnityEngine;

public interface IAnimationHandle
{
    void HandleSkill();
    void HandleSkillRecover();
}

public class PlayerAnimationController : MonoBehaviour, IAnimationHandle
{

    [Header("Setup")]
    [SerializeField, Required]
    private Animator animator;

    [SerializeField]
    private GhostTrailSystem ghostTrailSystem;


    //Privates
    private Action skillAction;
    private Action skillRecover;


    public void Move(float sqrMagnitude)
    {
        animator.SetFloat("Movement", sqrMagnitude);
    }


    public void Dash(bool dash)
    {
        ghostTrailSystem?.Animate(dash);
        animator.SetBool("Dash", dash);
    }

    public void Dodge(int direction)
    {
        animator.SetInteger("Dodge_Direction", direction);
        animator.SetTrigger("Dodge");
    }


    public void CastSkill(BaseSkill.AttackType attackType, int attackIndex, Action skillAction = null, Action skillRecover = null)
    {
        animator.SetInteger("Attack_Type", (int)attackType);
        animator.SetInteger("Attack_Index", attackIndex);
        animator.SetTrigger("Attack");

        this.skillAction = skillAction;
        this.skillRecover = skillRecover;
    }

    public void HandleSkill()
    {
        skillAction?.Invoke();
    }


    public void HandleSkillRecover()
    {
        skillRecover?.Invoke();
    }

}