﻿using System.Collections;
using Lean.Pool;
using UnityEngine;

public interface IHit
{
    void OnHit();
}

public class ProjectileController : MonoBehaviour, IHit
{
    [Header("References")]
    [SerializeField]
    private GameObject vfxHitPrefab;

    [SerializeField]
    private Material hitMaterial;

    [SerializeField, Min(0f)]
    private float blinkTime;

    [SerializeField]
    private TrailRenderer trailRenderer;

    private VelocitySystem velocitySystem;

    private void Awake()
    {
        velocitySystem = GetComponent<VelocitySystem>();
    }

    public void OnHit()
    {
        BlinkDamage();
        ReflectDirection();
        SpawnVFXHit();
    }

    private void BlinkDamage()
    {
        var view = transform.GetChild(0);

        Material cacheMaterial = view.GetComponent<Renderer>().material;
      
        StartCoroutine(BlinkMaterialCouroutine());
        IEnumerator BlinkMaterialCouroutine()
        {
            view.GetComponent<Renderer>().material = hitMaterial;

            yield return new WaitForSecondsRealtime(blinkTime);

            view.GetComponent<Renderer>().material = cacheMaterial;
        }

    }

    private void SpawnVFXHit()
    {
        var vfx = LeanPool.Spawn(vfxHitPrefab, transform.position, Quaternion.identity);
        vfx.GetComponent<Cinemachine.CinemachineImpulseSource>()?.GenerateImpulse(velocitySystem.GetDirection());

        LeanPool.Despawn(vfx, 2f);
    }


    private void ReflectDirection()
    {
        velocitySystem.Reflect(velocitySystem.GetDirection());
        transform.rotation = Quaternion.LookRotation(velocitySystem.GetDirection());
    }

    private void OnCollisionEnter(Collision collision)
    {

        var vfxHit = vfxHitPrefab;
        vfxHit.transform.GetChild(0).gameObject.SetActive(false); //Ripple Effect

        var damageEvent = new DamageEvent
        {
            Value = 1,
            Direction = gameObject.transform.position - collision.gameObject.transform.position,
            VFXPrefab = vfxHit
        };

        collision.gameObject.GetComponent<IDamage>()?.TakeDamage(damageEvent);

        LeanPool.Despawn(gameObject);
    }


    private void OnDisable()
    {
        trailRenderer.Clear();
    }

}
