﻿using UnityEngine;

public class PlayerDodgeController : MonoBehaviour
{

    [SerializeField]
    private LayerMask dodgeLayerMask;

    [SerializeField, Min(0f)]
    private float dodgeSize;

    [SerializeField]
    private AudioSource audioSource;

    [SerializeField]
    private AudioClip audioClip;

    //Privates
    private bool dodgingRight;
    private bool dodgingLeft;


    public delegate void Dodge(int diretion);
    public event Dodge OnDodge;


    private void Update()
    {
        CheckDodge();

    }

    private void CheckDodge()
    {

        if (Physics.CheckSphere(gameObject.transform.position + gameObject.transform.up + gameObject.transform.right, dodgeSize, dodgeLayerMask))
        {

            if (!dodgingLeft)
            {
                OnDodge?.Invoke(1);
                dodgingLeft = true;
                audioSource.PlayOneShot(audioClip);
            }
           
        }
        else
        {
            dodgingLeft = false;
        }

        if (Physics.CheckSphere(gameObject.transform.position + gameObject.transform.up + gameObject.transform.right * -1, dodgeSize, dodgeLayerMask))
        {
            if (!dodgingRight)
            {
                OnDodge?.Invoke(0);
                dodgingRight = true;
                audioSource.PlayOneShot(audioClip);
            }
            
        }
        else
        {
            dodgingRight = false;
        }

    }

    private void OnDrawGizmos()
    {
        Gizmos.DrawSphere(gameObject.transform.position + gameObject.transform.up + gameObject.transform.right, dodgeSize);
        Gizmos.DrawSphere(gameObject.transform.position + gameObject.transform.up + gameObject.transform.right * -1, dodgeSize);

    }
}