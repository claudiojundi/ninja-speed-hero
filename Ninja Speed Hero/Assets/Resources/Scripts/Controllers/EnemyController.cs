﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;


public struct DamageEvent
{
    public int Value;
    public Vector3 Direction;
    public GameObject VFXPrefab;

}

public interface IDamage
{
    void TakeDamage(DamageEvent damageEvent);
}


public class EnemyController : MonoBehaviour, IDamage
{

    [SerializeField]
    public GameObject target;

    [SerializeField]
    private float radiusOffset;

    private EnemyMovementController enemyMovementController;
    private EnemyAnimationController enemyAnimationController;
    private DamageTextInstantiator damageTextInstantiator;
    private SkillManager skillManager;

    private Tweener lockMovementTweener;

    void Start()
    {
        enemyAnimationController = GetComponent<EnemyAnimationController>();
        enemyMovementController = GetComponent<EnemyMovementController>();
        damageTextInstantiator = GetComponent<DamageTextInstantiator>();
        skillManager = GetComponent<SkillManager>();
    }


    public void SetTarget(GameObject target)
    {

    }

    private void Update()
    {
        enemyAnimationController.Move(enemyMovementController.isMoving);

        if (Input.GetKeyUp(KeyCode.Space))
        {
            Attack();
        }
    }


    public void TakeDamage(DamageEvent damageEvent)
    {
        enemyAnimationController.TakeDamage(damageEvent);
        enemyMovementController.KnockBack(damageEvent.Direction);
        damageTextInstantiator.Instantiate(damageEvent);

        lockMovementTweener.Kill(true);
        lockMovementTweener =  DOVirtual.Float(0f, 1f, 1f, (t) =>
          {

          }).OnStart(() =>
          {
              enemyMovementController.canMove = false;
          }).OnComplete(() => {

              enemyMovementController.canMove = true;
          });
    }


    [Button]
    public void Attack()
    {
         if (!CanCastSkill())
            return;

        var currentSkill = skillManager.currentSkill;

        skillManager.PrepareSkill();
        enemyAnimationController.CastSkill(currentSkill.attackType, currentSkill.attackIndex, () =>
        {
            skillManager.CastCurrentSkill();

        }, () =>
        {
            skillManager.SetRecover();

        });
    }


    private bool CanCastSkill()
    {
        bool canCast = true;

        if (!skillManager.CanCastSkill())// || health.Dead || status.Paralysed etc...
        {
            canCast = false;
        }

        return canCast;
    }

}
