﻿using DG.Tweening;
using Lean.Pool;
using TMPro;
using UnityEngine;

public class DamageTextInstantiator : MonoBehaviour
{

    public GameObject damageTextPrefab;



    public void Instantiate(DamageEvent damageEvent)
    {

        var damageText = LeanPool.Spawn(damageTextPrefab, transform.position + transform.up, Quaternion.identity);
        damageText.GetComponent<TextMeshPro>().text = damageEvent.Value.ToString();

        Animate(damageText, damageEvent.Direction);

    }

    private void Animate(GameObject damageText, Vector3 direction)
    {
        var initPos = transform.position + transform.up;

        damageText.transform.DOMove(initPos + direction * 2f, 1f).OnComplete(() =>
         {
             LeanPool.Despawn(damageText);
         });

        damageText.transform.DOShakeScale(1f, 0.3f);

    }
}