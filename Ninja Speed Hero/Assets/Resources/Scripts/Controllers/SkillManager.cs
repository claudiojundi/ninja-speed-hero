﻿using System.Collections.Generic;
using Sirenix.OdinInspector;
using UnityEngine;

public class SkillManager : MonoBehaviour
{

    public enum SkillManagerStatus
    {
        Idle,
        Preparation,
        Cast,
        Recover
    }



    [ReadOnly]
    public BaseSkill currentSkill;

    [ReadOnly]
    [SerializeField]
    private int currentSkillIndex;

    [SerializeField]
    private List<BaseSkill> skillList;


    [SerializeField, ReadOnly]
    private SkillManagerStatus _skillManagerStatus;
    public SkillManagerStatus skillManagerStatus
    {
        get { return _skillManagerStatus; }

        set
        {
            if (value != _skillManagerStatus)
            {
                _skillManagerStatus = value;
                OnStatusChanged?.Invoke(_skillManagerStatus);
            }

        }
    }

    public delegate void StatusChanged(SkillManagerStatus status);
    public StatusChanged OnStatusChanged;


    //Privates
    private float timeCountDown;


    private void Start()
    {
        SetupSkills();
        OnStatusChanged += HandleStatusChange;
    }


    private void HandleStatusChange(SkillManagerStatus status)
    {

    }

    private void SetupSkills()
    {

        if (skillList.Count > 0)
        {
            List<BaseSkill> skillListCache = new List<BaseSkill>();

            for (int skillIndex = 0; skillIndex < skillList.Count; skillIndex++)
            {
                var skillCopy = Instantiate(skillList[skillIndex]);
                skillCopy.Setup(gameObject);

                skillListCache.Add(skillCopy);
            }

            skillList = skillListCache;
            currentSkill = skillList[0];
        }


    }

    public void PrepareSkill()
    {
        skillManagerStatus = SkillManagerStatus.Preparation;
    }

    public void CastCurrentSkill()
    {
        skillManagerStatus = SkillManagerStatus.Cast;
        currentSkill.Perform(Vector3.zero); //if current skill is projectile caster...?
    }


    public void SetRecover()
    {
        skillManagerStatus = SkillManagerStatus.Recover;

        currentSkillIndex = (++currentSkillIndex % skillList.Count);
        currentSkill = skillList[currentSkillIndex];
    }

    public bool CanCastSkill()
    {
        return (skillManagerStatus == SkillManagerStatus.Idle || skillManagerStatus == SkillManagerStatus.Recover);
    }



    private void Update()
    {
        //RecoverTimeCountDown();
    }


    private void RecoverTimeCountDown()
    {
        if (skillManagerStatus == SkillManagerStatus.Recover)
        {
            if (timeCountDown <= currentSkill.recoverTime)
            {
                timeCountDown += Time.deltaTime;
            }
            else
            {
                skillManagerStatus = SkillManagerStatus.Idle;

                currentSkillIndex = 0;
                timeCountDown = 0f;
            }
        }
    }


    [Button]
    private void CastSkillTest(int index)
    {
        skillList[index].Perform(Vector3.zero);
    }

}
