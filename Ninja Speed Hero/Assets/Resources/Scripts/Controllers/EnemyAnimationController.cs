﻿using System;
using System.Collections;
using DG.Tweening;
using Lean.Pool;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

public class EnemyAnimationController : MonoBehaviour, IAnimationHandle
{

    [SerializeField]
    private Animator animator;

    [SerializeField, Required]
    private SkinnedMeshRenderer[] viewsSkinnedMesh;

    [SerializeField]
    private Material viewMaterial;

    [SerializeField]
    private Material damageMaterial;

    [SerializeField, Min(0f)]
    private float blinkTime;

    //Privates
    private Vector3 lookPosition;

    public Tween punchScaleTween;

    public UnityEvent testeEvent;

    //Privates
    private Action skillAction;
    private Action skillRecover;

    private bool oldIsMoving;


    private void Start()
    {

    }


    public void Move(bool isMoving)
    {
        if (oldIsMoving != isMoving)
        {
            float from = animator.GetFloat("Movement");
            float to = 1f;

            if (!isMoving)
                to = 0f;

            DOVirtual.Float(from, to, 0.25f, (t) =>
            {
                animator.SetFloat("Movement", t);
            });

            oldIsMoving = isMoving;
        }




    }


    public void CastSkill(BaseSkill.AttackType attackType, int attackIndex, Action skillAction = null, Action skillRecover = null)
    {
        animator.SetInteger("Attack_Type", (int)attackType);
        animator.SetInteger("Attack_Index", attackIndex);
        animator.SetTrigger("Attack");

        this.skillAction = skillAction;
        this.skillRecover = skillRecover;
    }


    public void HandleSkill()
    {
        skillAction?.Invoke();
    }


    public void HandleSkillRecover()
    {
        skillRecover?.Invoke();
    }



    [Button]
    public void TakeDamage(DamageEvent damageEvent)
    {
        animator.SetTrigger("Hit");

        BlinkDamage();
        PunchScaleDamage();
        InstantiateDamageVFX(damageEvent);

        testeEvent?.Invoke();
    }



    private void BlinkDamage()
    {
        StartCoroutine(BlinkMaterialCouroutine());
        IEnumerator BlinkMaterialCouroutine()
        {
            for (int skinnedMeshindex = 0; skinnedMeshindex < viewsSkinnedMesh.Length; skinnedMeshindex++)
            {
                viewsSkinnedMesh[skinnedMeshindex].material = damageMaterial;
            }

            yield return new WaitForSecondsRealtime(blinkTime);

            for (int skinnedMeshindex = 0; skinnedMeshindex < viewsSkinnedMesh.Length; skinnedMeshindex++)
            {
                viewsSkinnedMesh[skinnedMeshindex].material = viewMaterial;
            }
        }


    }



    private void PunchScaleDamage()
    {
        punchScaleTween.Kill(true);
        punchScaleTween = transform.DOPunchScale(Vector3.one * 0.2f, 0.2f).OnComplete(() =>
        {
            transform.localScale = Vector3.one;
        });


    }

    private void InstantiateDamageVFX(DamageEvent damageEvent)
    {
        if (damageEvent.VFXPrefab)
        {
            var vfx = LeanPool.Spawn(damageEvent.VFXPrefab, transform.position + transform.up, Quaternion.identity);
            vfx.GetComponent<Cinemachine.CinemachineImpulseSource>()?.GenerateImpulse(damageEvent.Direction);

            LeanPool.Despawn(vfx, 1f);

        }
    }


}
