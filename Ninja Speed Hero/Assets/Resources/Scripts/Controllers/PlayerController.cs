﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class MovementMagnitudeEvent : UnityEvent<float> { }


public class PlayerController : MonoBehaviour
{
    [Header("References")]
    [SerializeField, InlineEditor]
    private JoystickValue joystickValue;



    [Header("Events")]
    [SerializeField]
    private MovementMagnitudeEvent movementMagnitudeEvent;


    private PlayerMovementController playerMovementController;
    private PlayerAnimationController playerAnimationController;
    private PlayerDodgeController playerDodgeController;
    private SkillManager skillManager;

    //private TimeSystem timeSystem;

    private Vector3 movement;
    private bool canEventMagnitude = true;
    private float magnitudeCountdown;
    private bool magnitudeCountdownComplete;



    private void Awake()
    {
        playerMovementController = GetComponent<PlayerMovementController>();
        playerAnimationController = GetComponent<PlayerAnimationController>();
        playerDodgeController = GetComponent<PlayerDodgeController>();
        
        skillManager = GetComponent<SkillManager>();

    }

    private void Start()
    {
        playerDodgeController.OnDodge += HandleDodge;
    }

    private void HandleDodge(int diretion)
    {
        playerAnimationController.Dodge(diretion);
    }

    private void Update()
    {
        MovePlayer();
        MagnitudeCountdown();
    }

    private void MovePlayer()
    {

        if (joystickValue)
        {
            movement = new Vector3(joystickValue.Horizontal, 0f, joystickValue.Vertical);

            //if (!holding)
            //{
            if (movement.sqrMagnitude > 0.1f)
            {
                playerMovementController.Turning(movement.x, movement.z);

            }

            playerAnimationController.Move(movement.magnitude);
            playerMovementController.Move(movement);
            //}

            //else
            //{
            //    playerMovementController.Turning(movement.x, movement.z);
            //}

            if (canEventMagnitude)
                movementMagnitudeEvent?.Invoke(Mathf.Clamp(movement.sqrMagnitude, 0.25f, 1f));
        }



    }

    private void MagnitudeCountdown()
    {
        if (!canEventMagnitude && !magnitudeCountdownComplete)
        {
            magnitudeCountdown -= Time.unscaledDeltaTime;

            if (magnitudeCountdown <= 0f)
            {
                magnitudeCountdownComplete = true;

                if (movement.sqrMagnitude > 0.1f)
                {
                    canEventMagnitude = true;
                    magnitudeCountdownComplete = false;
                }
                else
                {
                    DOVirtual.Float(1f, 0f, 0.2f, (t) =>
                    {
                        movementMagnitudeEvent?.Invoke(t);

                    }).OnComplete(() =>
                    {
                        canEventMagnitude = true;
                        magnitudeCountdownComplete = false;
                    });
                }
            }
        }
    }

    private bool CanCastSkill()
    {
        bool canCast = true;

        if (!skillManager.CanCastSkill())// || health.Dead || status.Paralysed etc...
        {
            canCast = false;
        }

        return canCast;
    }


    public void HandleTap(Vector3 position)
    {

        if (!CanCastSkill())
            return;

        var currentSkill = skillManager.currentSkill;


        canEventMagnitude = false;
        movementMagnitudeEvent?.Invoke(1f);
        magnitudeCountdown = 2f;


        skillManager.PrepareSkill();
        playerAnimationController.CastSkill(currentSkill.attackType, currentSkill.attackIndex, () =>
        {
            skillManager.CastCurrentSkill();

        }, () =>
        {
            skillManager.SetRecover();

        });
    }

    public void HandleSwipe(Vector3 position)
    {

        return;


        playerMovementController.Dash(null, () =>
        {
            canEventMagnitude = false;
            movementMagnitudeEvent?.Invoke(1f);
            magnitudeCountdown = 2f;

            playerAnimationController.Dash(true);

        }, () =>
        {
            playerAnimationController.Dash(false);

            //canEventMagnitude = true;
            //magnitudeCountdownComplete = false;

            //movementMagnitudeEvent?.Invoke(1f);

        });



    }





}
