﻿using UnityEngine;
using UnityEngine.Rendering.PostProcessing;


public class CameraController : MonoBehaviour
{

    [Header("References")]
    [SerializeField]
    private PostProcessVolume slowMotionPostProcessVolume;




    public void SetSlowMotionEffect(float effectValue)
    {
        float oppositeValue = 1f - effectValue;
        slowMotionPostProcessVolume.weight = oppositeValue;
    }

}
