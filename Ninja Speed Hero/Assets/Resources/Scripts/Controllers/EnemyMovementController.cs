﻿using System;
using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;

public class EnemyMovementController : MonoBehaviour
{

    public bool isMoving;

    public bool canMove = true;

    [SerializeField, ReadOnly]
    private Vector3 destination;

    [MinValue(0f)]
    public float moveSpeedForward;

    [MinValue(0f)]
    public float moveSpeedBackwards;

    [SerializeField, Min(0f)]
    private float knockBackDuration;
    [SerializeField]
    private AnimationCurve knockBackEase;

    [SerializeField, MinValue(0f)]
    private float lookToSpeed;


    //Privates
    private Tweener knockBackTweener;
    private Tweener moveTweener;
    private Tweener aimTweener;
    private Vector3 oldDestination;
    private bool forceStop;



    public void SetDestination(Vector3 destination)
    {
        this.destination = destination;
    }



    public void Move(Vector3 destination, float speed = 0f, Action onComplete = null)
    {
        if (!canMove)
        {
            moveTweener.Kill(false);
            return;
        }

        if ((oldDestination != destination) || forceStop)
        {
            isMoving = true;

            moveTweener.Kill(false);
            moveTweener = transform.DOMove(destination, speed).SetSpeedBased(true).OnComplete(() =>
            {
                isMoving = false;
                onComplete?.Invoke();
            });

            forceStop = false;
            oldDestination = destination;
        }

    }


    public void LookTo(Vector3 point)
    {
        aimTweener.Kill();
        aimTweener = transform.DOLookAt(point, lookToSpeed).SetSpeedBased(true);
    }


    public void KnockBack(Vector3 direction)
    {
        knockBackTweener.Kill(false);
        direction *= 0.7f;

        var knockBackDirection = new Vector3(direction.x, 0f, direction.z) + gameObject.transform.position;

        knockBackTweener = transform.DOMove(knockBackDirection, knockBackDuration).SetEase(knockBackEase);

    }

    public void StopMove()
    {
        if (isMoving)
            moveTweener.Kill(false);
    }

    public void ForceStopMove()
    {
        forceStop = true;
        StopMove();
    }

}
