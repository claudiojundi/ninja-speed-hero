﻿using UnityEngine;

public class DebugManager : MonoBehaviour
{
    
    public bool useSceneView;


    private void Awake()
    {
        if (useSceneView)
        {
#if UNITY_EDITOR
            UnityEditor.EditorWindow.FocusWindowIfItsOpen(typeof(UnityEditor.SceneView));
#endif
        }
    }
}
