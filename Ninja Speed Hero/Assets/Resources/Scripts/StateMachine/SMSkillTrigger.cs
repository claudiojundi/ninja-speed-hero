﻿using UnityEngine;

public class SMSkillTrigger : StateMachineBehaviour
{


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<IAnimationHandle>().HandleSkill();
    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        animator.GetComponentInParent<IAnimationHandle>().HandleSkillRecover();
    }

}
