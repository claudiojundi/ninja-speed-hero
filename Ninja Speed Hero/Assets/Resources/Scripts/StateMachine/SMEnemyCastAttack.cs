﻿using UnityEngine;

public class SMEnemyCastAttack : StateMachineBehaviour
{

    public string castCompleteParamName;

    //Privates
    private EnemyController enemyController;
    private SkillManager skillManager;


    public override void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyController = animator.GetComponentInParent<EnemyController>();
        skillManager = animator.GetComponentInParent<SkillManager>();
    }


    public override void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (stateInfo.normalizedTime >= skillManager.currentSkill.castTime)
        {
            animator.SetTrigger(castCompleteParamName);
        }
    }


    public override void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyController.Attack();
    }

}
