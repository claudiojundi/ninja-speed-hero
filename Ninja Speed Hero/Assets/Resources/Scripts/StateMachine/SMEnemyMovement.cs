﻿using UnityEngine;

public class SMEnemyMovement : StateMachineBehaviour
{
    public string moveCompleteParamName;
    public string moveDirectionParamName;

    //Privates
    private EnemyController enemyController;
    private EnemyMovementController enemyMovementController;
    private SkillManager skillManager;

    private const float MovementDirectionTransitionSpeed = 2.5f;


    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        enemyController = animator.GetComponentInParent<EnemyController>();
        enemyMovementController = animator.GetComponentInParent<EnemyMovementController>();
        skillManager = animator.GetComponentInParent<SkillManager>();

        var destination = enemyController.target.transform.position + (enemyController.gameObject.transform.position - enemyController.target.transform.position).normalized * skillManager.currentSkill.minDistanceToAttack;
        if ((enemyController.transform.position - destination).sqrMagnitude <= 0.1f)
        {
            animator.SetTrigger(moveCompleteParamName);
        }
    }

    override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {

        /*if (walkingComplete)
            return;*/

        var init = enemyController.gameObject.transform.position;
        var final = enemyController.target.transform.position;

        var distance = Vector3.Distance(init, final);
        var direction = ((final - init).normalized * -1);


        enemyMovementController.LookTo(enemyController.target.transform.position);

        if (distance >= skillManager.currentSkill.minDistanceToAttack)
        {
            MoveForward(animator, final, direction);
        }

        else
        {
            MoveBackwards(animator, init, final, direction);
        }

    }

    private void MoveForward(Animator animator, Vector3 final, Vector3 direction)
    {

        var moveDirectionValue = Mathf.Lerp(animator.GetFloat(moveDirectionParamName), 1f, Time.deltaTime * MovementDirectionTransitionSpeed);
        animator.SetFloat(moveDirectionParamName, moveDirectionValue);


        var pos = final + direction * skillManager.currentSkill.minDistanceToAttack;

        enemyMovementController.Move(pos, enemyMovementController.moveSpeedForward, () =>
         {
             animator.SetTrigger(moveCompleteParamName);
         });
    }

    private void MoveBackwards(Animator animator, Vector3 init, Vector3 final, Vector3 direction)
    {

        var moveDirectionValue = Mathf.Lerp(animator.GetFloat(moveDirectionParamName), 0f, Time.deltaTime * MovementDirectionTransitionSpeed);
        animator.SetFloat(moveDirectionParamName, moveDirectionValue);
        

        final = enemyController.gameObject.transform.position;
        init = enemyController.target.transform.position;

        direction = ((final - init).normalized);

        var pos = final + direction * skillManager.currentSkill.minDistanceToAttack;

        enemyMovementController.Move(pos, enemyMovementController.moveSpeedBackwards, () =>
         {
             animator.SetTrigger(moveCompleteParamName);
         });

    }


    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (enemyMovementController.isMoving)
        {
            enemyMovementController.ForceStopMove();
        }
        else
        {
            enemyMovementController.StopMove();
        }
    }

}
