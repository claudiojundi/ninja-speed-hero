﻿using UnityEngine;

public enum StateMachineStates
{
    Enter,
    Exit,
}


public class SMRandomizeInt : StateMachineBehaviour
{

    public string paramName;
    public StateMachineStates when;
    public int randomMax;


    // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
    override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (when == StateMachineStates.Enter)
        {
            animator.SetInteger(paramName, Random.Range(0, randomMax));
        }
    }

    //OnStateExit is called when a transition ends and the state machine finishes evaluating this state
    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
    {
        if (when == StateMachineStates.Exit)
        {
            animator.SetInteger(paramName, Random.Range(0, randomMax));
        }
    }
}