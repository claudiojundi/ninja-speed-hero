﻿using Sirenix.OdinInspector;
using UnityEngine;

public class FollowTargetSystem : MonoBehaviour
{
    [SerializeField]
    private bool canFollow = true;

    [SerializeField, Required]
    private Transform target;

    [SerializeField, MinValue(0f)]
    private float speed;

    [SerializeField]
    private Vector3 offset;


    public void CanFollow(bool canFollow)
    {
        this.canFollow = canFollow;
    }



    private void Update()
    {
        if (canFollow)
            gameObject.transform.position = Vector3.Lerp(transform.position, target.transform.position + offset, Time.unscaledDeltaTime * speed);
    }
}
