﻿using Sirenix.OdinInspector;
using UnityEngine;

[RequireComponent(typeof(Rigidbody))]
public class VelocitySystem : MonoBehaviour
{
    public bool canMove = true;

    [EnableIf("canMove")]
    [SerializeField]
    [Range(0f, 100f)]
    private float speed;

    [Header("Info")]
    [EnableIf("canMove")]
    [SerializeField]
    private Vector3 movementDirection = Vector3.zero;

    //Privates
    private new Rigidbody rigidbody;


    private void Awake()
    {
        rigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
        Move();
    }

    private void Move()
    {
        if (canMove)
            rigidbody.velocity = movementDirection * speed;
    }

    public void EnableMovement(bool canMove)
    {
        this.canMove = canMove;
    }
    public void SetDirection(Vector3 position)
    {
        this.movementDirection = position.normalized;

    }
    public Vector3 GetDirection()
    {
        return this.movementDirection;
    }

    public void SetSpeed(float speed)
    {
        this.speed = speed;
    }

    public void Reflect(Vector3 normal)
    {
        Vector3 reflection = Vector3.Reflect(GetDirection(), normal);
        SetDirection(reflection);
    }
}




