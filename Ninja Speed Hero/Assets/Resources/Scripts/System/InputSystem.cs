﻿using Sirenix.OdinInspector;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

[System.Serializable]
public class InputEvent : UnityEvent<Vector3> { }

[System.Serializable]
public class SwipeEvent : UnityEvent<Vector3, float> { }


public class InputSystem : MonoBehaviour, IPointerDownHandler, IDragHandler, IEndDragHandler, IPointerUpHandler
{

    public enum InputStatus
    {
        None,
        Click,
        PointerUp,
        PointerDown,
        DragStart,
        Drag,
        DragEnd,
    }

    [ReadOnly]
    public InputStatus inputStatus;

    [Header("Setup")]
    [SerializeField]
    private bool canInput = true;

    [SerializeField]
    private bool multiTouchEnable;


    [SerializeField, MinValue(0f)]
    private float tapTime;

    [SerializeField, MinValue(0f)]
    private float swipeTime;


    [Space]
    [EnableIf("canInput")]
    public InputEvent onDown;
    [EnableIf("canInput")]
    public InputEvent onDrag;
    [EnableIf("canInput")]
    public InputEvent onSwipe;
    [EnableIf("canInput")]
    public InputEvent onUp;
    [EnableIf("canInput")]
    public InputEvent onTap;


    [Space]
    public UnityEvent onHold;

    private bool touching;

    [SerializeField, ReadOnly]
    private bool holding;
    [SerializeField, ReadOnly]
    private float holdTime = 0f;

    [SerializeField, ReadOnly]
    private float currentSwipeTime;


    [SerializeField]
    private Joystick joystick;

    [SerializeField, ShowIf("joystick")]
    private JoystickValue joystickValue;


    


    private void Awake()
    {
        Input.multiTouchEnabled = multiTouchEnable;
    }

    private void Start()
    {
        ResetTouchStatus();
    }

    [Button]
    public void Click()
    {
        inputStatus = InputStatus.PointerDown;

        touching = true;
        onDown.Invoke(Vector3.zero);


        if ((currentSwipeTime < swipeTime) && inputStatus == InputStatus.Drag)
        {
            onSwipe.Invoke(Vector3.zero);
        }

        else
        {
            if (inputStatus != InputStatus.Drag)
                joystick.TapAnimation();

            inputStatus = InputStatus.PointerUp;
            onUp.Invoke(Vector3.zero);
        }

        ResetTouchStatus();
    }


    public void OnPointerDown(PointerEventData eventData)
    {
        inputStatus = InputStatus.PointerDown;

        joystick.InputDownAnimation();

        touching = true;
        onDown.Invoke(eventData.position);
    }


    public void OnPointerUp(PointerEventData eventData)
    {

        if ((currentSwipeTime < swipeTime) && inputStatus ==  InputStatus.Drag)
        {
           onSwipe.Invoke(eventData.position);
        }

        else if (inputStatus != InputStatus.Drag)
        {
            inputStatus = InputStatus.PointerUp;
            onUp.Invoke(eventData.position);
        }


        joystick.InputUpAnimation();
        ResetTouchStatus();

    }


    public void OnDrag(PointerEventData eventData)
    {
        inputStatus = InputStatus.Drag;

        if (currentSwipeTime > swipeTime)
            onDrag.Invoke(eventData.position);
    }



    public void OnEndDrag(PointerEventData eventData) { }


    private void LateUpdate()
    {
        JoyStickValues();
        CheckHold();
        CheckSwipe();
    }

   

    private void JoyStickValues()
    {

        if (joystick)
        {
            var horizontal = Mathf.Clamp(joystick.Horizontal, -1.0f, 1.0f);
            var vertical = Mathf.Clamp(joystick.Vertical, -1.0f, 1.0f);

            joystickValue.Horizontal = horizontal;
            joystickValue.Vertical = vertical;
        }

    }

    private void CheckHold()
    {
        if (touching)
        {
            if (holdTime < tapTime)
            {
                if (joystick)
                {
                    var joystickMagnitude = new Vector2(joystick.Horizontal, joystick.Vertical).normalized.sqrMagnitude;

                    if (joystickMagnitude > 0f)
                    {
                        holdTime = 0f;
                        holding = false;
                    }
                }

                holdTime += Time.unscaledDeltaTime; 
            }
            else
            {
                if (!holding)
                {
                    joystick.HoldAnimation();
                    onHold.Invoke();
                }

                holding = true;
            }
        }
    }

    private void CheckSwipe()
    {

        if (inputStatus == InputStatus.Drag)
            currentSwipeTime += Time.unscaledDeltaTime;

    }

    private void ResetTouchStatus()
    {
        touching = false;
        holding = false;

        holdTime = 0f;
        currentSwipeTime = 0f;

        inputStatus = InputStatus.None;

    }


}
