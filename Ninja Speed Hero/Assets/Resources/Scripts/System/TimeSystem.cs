﻿using DG.Tweening;
using Sirenix.OdinInspector;
using UnityEngine;


public class TimeSystem : MonoBehaviour
{

    [SerializeField, ReadOnly]
    private float timeScale;

    [SerializeField, Min(0f)]
    private float freezeFrameDuration = 0.1f;

    //Privates
    private Tween freezeTween;


    public void ChangeTimeScale(float timeScale)
    {
        this.timeScale = Mathf.Clamp(timeScale, 0.1f, 1f);
        Time.timeScale = this.timeScale;
    }


    [Button]
    public void FreezeFrame()
    {
        Time.timeScale = 0.2f;


        freezeTween = DOVirtual.Float(Time.timeScale, 1f, freezeFrameDuration, (float t) =>
        {

        }).SetUpdate(true).OnComplete(() =>
        {
            Time.timeScale = 1f;
        });

    }


}
