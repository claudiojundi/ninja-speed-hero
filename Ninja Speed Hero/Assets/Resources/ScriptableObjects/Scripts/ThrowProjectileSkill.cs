﻿using UnityEngine;
using Lean.Pool;
using Sirenix.OdinInspector;

[CreateAssetMenu(fileName = "Skills", menuName = "Skill/Throw Projectile Skill")]
public class ThrowProjectileSkill : BaseSkill
{

    [Header("Throw Projectile Setups")]
    [SerializeField]
    private GameObject projectilePrefab;

    [SerializeField, Min(1)]
    private int projectileLength;

    [SerializeField]
    private Vector3 spawnLocalPosition;


   

    //privates
    private GameObject gameObject;


    public override void Setup(GameObject gameObject)
    {
        this.gameObject = gameObject;

    }


    public override void Perform(Vector3 initPosition)
    {
        //Debug.Log("Throw Projectile Skill");

        var sides = gameObject.transform.right * spawnLocalPosition.x;
        var height = gameObject.transform.up * spawnLocalPosition.y;
        var front = gameObject.transform.forward * spawnLocalPosition.z;

        if (projectileLength == 1)
        {
            var projectile = LeanPool.Spawn(projectilePrefab, gameObject.transform.position + front + height + sides, Quaternion.LookRotation(gameObject.transform.forward, gameObject.transform.up));
            projectile.GetComponent<VelocitySystem>().SetDirection(gameObject.transform.forward);

            LeanPool.Despawn(projectile, 10f);
        }

        else
        {
            Ray ray0 = CreateRayPath(gameObject.transform.position + front + height + sides, 10f);
            Ray ray1 = CreateRayPath(gameObject.transform.position + front + height + sides, 1f);
            Ray ray2 = CreateRayPath(gameObject.transform.position + front + height + sides, -10f);

            Debug.DrawLine(ray0.origin, ray0.origin + ray0.direction * 10f, Color.red, 3f);
            Debug.DrawLine(ray1.origin, ray1.origin + ray1.direction * 10f, Color.red, 3f);
            Debug.DrawLine(ray2.origin, ray2.origin + ray2.direction * 10f, Color.red, 3f);

            Ray[] rays = { ray0, ray1, ray2 };

            for (int i = 0; i < rays.Length; i++)
            {
                var projectile = LeanPool.Spawn(projectilePrefab, gameObject.transform.position + front + height + sides, Quaternion.LookRotation(rays[i].direction));
                projectile.GetComponent<VelocitySystem>().SetDirection(rays[i].direction);

                LeanPool.Despawn(projectile, 10f);
            }
        }
    }

    private Ray CreateRayPath(Vector3 shootPoint, float angleValue)
    {
        Ray ray = new Ray();
        ray.origin = shootPoint;
        ray.direction = gameObject.transform.forward;
        ray.direction = Quaternion.AngleAxis(angleValue, gameObject.transform.up) * ray.direction;

        return ray;
    }

}
