﻿using DG.Tweening;
using UnityEngine;

[CreateAssetMenu(fileName = "Skills", menuName = "Skill/Melee")]
public class MeleeSkill : BaseSkill
{

    [SerializeField]
    private LayerMask layerMask;


    [Header("Melee Setups")]

    [Min(0)]
    public int damage;
    public Vector3 halfExtendsSize = new Vector3(0.5f, 0.5f, 2f);

    public GameObject vfxDamagePrefab;

    [SerializeField]
    private LayerMask enemyLayerMask;

    //Privates
    private GameObject gameObject;
    private IDash iDash;


    public override void Setup(GameObject gameObject)
    {
        this.gameObject = gameObject;
        iDash = gameObject.GetComponent<IDash>();
    }

    public override void Perform(Vector3 position)
    {

        var closestEnemy = GetClosestEnemy();

        var attackPosition = closestEnemy ? closestEnemy.transform.position : gameObject.transform.position + gameObject.transform.forward;

        Vector3 direction = attackPosition - gameObject.transform.position;
        gameObject.transform.DORotateQuaternion(Quaternion.LookRotation(direction), 0f).OnComplete(()=>
        {

            Ray ray = new Ray
            {
                origin = gameObject.transform.position,
                direction = gameObject.transform.forward
            };

            iDash?.DashTo(gameObject.transform.position + gameObject.transform.forward * 0.6f);

            var centerPosition = gameObject.transform.position + Vector3.up + gameObject.transform.forward * halfExtendsSize.z;

            ExtDebug.DrawBoxCastBox(centerPosition, halfExtendsSize, Quaternion.LookRotation(ray.direction), ray.direction, 0f, Color.red, 1f);


            var damageEvent = new DamageEvent
            {
                Value = damage,
                VFXPrefab = vfxDamagePrefab,
                Direction = ray.direction
            };

            Collider[] colliders = Physics.OverlapBox(centerPosition, halfExtendsSize, Quaternion.LookRotation(ray.direction), layerMask);


            for (int colliderIndex = 0; colliderIndex < colliders.Length; colliderIndex++)
            {
                colliders[colliderIndex].transform.GetComponent<IHit>()?.OnHit();
                colliders[colliderIndex].transform.GetComponent<IDamage>()?.TakeDamage(damageEvent);
            }

        });


        //==


       

    }




    public GameObject GetClosestEnemy()
    {
        var enemies = Physics.OverlapSphere(gameObject.transform.position, 10f, enemyLayerMask);

        GameObject closestEnemy = null;

        float closestPoint = Mathf.Infinity;

        for (int i = 0; i < enemies.Length; i++)
        {
            var position = (gameObject.transform.position - enemies[i].transform.position).sqrMagnitude;

            if (closestPoint > position)
            {
                closestEnemy = enemies[i].gameObject;
                closestPoint = position;
            }
        }


        return closestEnemy;
    }

}