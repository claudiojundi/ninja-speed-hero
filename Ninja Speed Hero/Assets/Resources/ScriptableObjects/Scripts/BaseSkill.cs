﻿using Sirenix.OdinInspector;
using UnityEngine;




public abstract class BaseSkill : ScriptableObject
{

    public enum AttackType
    {
        Melee = 0,
        Ranged = 1,
    }

    public string skillName;

    public AttackType attackType;

    [MinValue(0f)]
    public int attackIndex;

    [MinValue(0f)]
    public float recoverTime;

    [MinValue(0f)]
    public float castTime;

    public bool isNPC = false;

    [ShowIf("isNPC")]
    public float minDistanceToAttack = 1f;


    public abstract void Setup(GameObject gameObject);
    public abstract void Perform(Vector3 initPosition);




}
