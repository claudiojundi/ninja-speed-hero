﻿using Sirenix.OdinInspector;
using UnityEngine;


[CreateAssetMenu(fileName = "Joystick", menuName = "ScriptableObjects/Joystick")]
public class JoystickValue : ScriptableObject
{
    [MinValue(-1f), MaxValue(1f)]
    public float Horizontal;

    [MinValue(-1f), MaxValue(1f)]
    public float Vertical;

}

