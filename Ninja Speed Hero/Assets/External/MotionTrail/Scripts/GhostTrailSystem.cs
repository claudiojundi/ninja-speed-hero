﻿using Lean.Pool;
using Sirenix.OdinInspector;
using UnityEngine;


public class GhostTrailSystem : MonoBehaviour
{
    [SerializeField, Required]
    private SkinnedMeshRenderer targetSkinnedMeshRenderer;

    [SerializeField, Required]
    private GameObject ghostPrefab;

    [SerializeField, MinValue(0f), Tooltip("Do not abuse")]
    private float trailGhostDuration = 1f;

    public bool animate;

    [SerializeField, MinValue(0f), EnableIf("animate")]
    private float animationTime;
    private float currentAnimationTime;


    private void Update()
    {
        if (animate)
        {
            currentAnimationTime += Time.deltaTime;

            if (currentAnimationTime > animationTime)
            {
                SpawnTrail();
                currentAnimationTime = 0f;
            }
        }
    }

    public void Animate(bool animate)
    {
        this.animate = animate;
    }

    [Button]
    public void SpawnTrail()
    {
        Mesh ghostMesh = new Mesh();
        targetSkinnedMeshRenderer.BakeMesh(ghostMesh);

        var ghostObject = LeanPool.Spawn(ghostPrefab
                                        , gameObject.transform.position
                                        , targetSkinnedMeshRenderer.gameObject.transform.rotation);

        var ghostTrail = ghostObject.GetComponent<GhostTrail>();
        ghostTrail.SetMesh(ghostMesh);
        ghostTrail.duration = trailGhostDuration;

        LeanPool.Despawn(ghostObject, trailGhostDuration);
    }

}
