﻿using DG.Tweening;
using UnityEngine;

[RequireComponent(typeof(MeshRenderer), typeof(MeshFilter))]
public class GhostTrail : MonoBehaviour
{

    private MeshRenderer meshRenderer;
    private MeshFilter meshFilter;

    [HideInInspector] public float duration;


    private void Awake()
    {
        meshRenderer = GetComponent<MeshRenderer>();
        meshFilter = GetComponent<MeshFilter>();
    }

    private void OnEnable()
    {
        meshRenderer.material.DOFloat(0f, "_NoiseScale", duration).OnComplete(()=>
        {
            meshRenderer.material.SetFloat("_NoiseScale", 0.5f);
        });
    }

    public void SetMesh(Mesh mesh)
    {
        this.meshFilter.mesh = mesh;
    }
}
